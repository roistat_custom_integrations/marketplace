<?php

namespace RsMarketplace\Calltouch;

use RsMarketplace\SDK;

class Application extends SDK\AbstractApplication implements SDK\Engine\ReceiveRequest {

    const EXPIRE_CACHE_TIME = 21600; // 6 hours

    /**
     * @var string[]
     */
    private $_validStatuses = [
        'successful'   => 'ANSWER',
        'unsuccessful' => 'NOANSWER'
    ];

    /**
     * @param SDK\Object\Request $request
     * @return string
     */
    public function receiveRequest(SDK\Object\Request $request) {
        $duration = $request->get("duration");
        $caller   = $request->get("callerphone");
        $callee   = $request->get("phonenumber");
        $callId   = $request->get("id");

        if (mb_strlen($caller) === 0) {
            return $this->_error('Caller is undefined');
        }
        if (mb_strlen($callee) === 0) {
            return $this->_error('Callee is undefined');
        }
        if ($callId === null) {
            return $this->_error('call_id is undefined');
        }

        if ($duration === null) {
            $this->_processStartCall($request);
        } else {
            $this->_processEndCall($request);
        }

        return $this->_success('Event was successfully processed');
    }

    /**
     * @param SDK\Object\Request $request
     */
    public function _processStartCall(SDK\Object\Request $request) {
        $outerCallId = $request->get("id");
        $leadId = $this->_createLead($request);
        $callId = $this->_createCall($request, $leadId);
        $this->_temporaryStorage->set($outerCallId, $callId, self::EXPIRE_CACHE_TIME);
    }

    /**
     * @param SDK\Object\Request $request
     */
    private function _processEndCall(SDK\Object\Request $request) {
        $call = $this->_getStoredCall($request);
        if ($call === null) {
            $leadId = $this->_createLead($request);
            $this->_createCall($request, $leadId);
        } else {
            $this->_updateCall($call, $request);
        }
    }

    /**
     * @param SDK\Object\Request $request
     * @return string
     */
    private function _createLead(SDK\Object\Request $request) {
        $caller = $request->get("callerphone");
        $date   = $request->get("timestamp");
        $visit  = $this->_getVisit($request->get("url"));
        $fields = [new SDK\Object\FieldValue('roistat', $visit)];

        return $this->_leads->send(new SDK\Object\Lead(
            null,
            null,
            null,
            t("New lead from calltracking"),
            null,
            null,
            $caller,
            null,
            $fields,
            new \DateTime($date)
        ));
    }

    /**
     * @param SDK\Object\Request $request
     * @param string $leadId
     * @return int
     */
    private function _createCall(SDK\Object\Request $request, $leadId) {
        $caller         = $request->get("callerphone");
        $callee         = $request->get("phonenumber");
        $date           = $request->get("timestamp");
        $visit          = $this->_getVisit($request->get("url"));
        $primaryStatus  = $request->get("status");
        $status         = $this->_checkStatus($primaryStatus) ? $this->_collationStatus($primaryStatus) : null;
        $duration       = $request->get("duration");
        $link           = $request->get("reclink");

        return $this->_callTracking->storeCall(new SDK\Object\CallTracking\Call(
            $callee,
            $caller,
            new \DateTime($date),
            $visit,
            null,
            $status,
            $leadId,
            $duration,
            $link
        ));
    }

    /**
     * @param SDK\Object\CallTracking\Call $call
     * @param SDK\Object\Request $request
     */
    private function _updateCall(SDK\Object\CallTracking\Call $call, SDK\Object\Request $request) {
        $visit          = $this->_getVisit($request->get("url"));
        $primaryStatus  = $request->get("status");
        $status         = $this->_checkStatus($primaryStatus) ? $this->_collationStatus($primaryStatus) : null;
        $duration       = $request->get("duration");
        $link           = $request->get("reclink");

        $this->_callTracking->storeCall(new SDK\Object\CallTracking\Call(
            $call->getCallee(),
            $call->getCaller(),
            $call->getDate(),
            $visit,
            $call->getMarker(),
            $status,
            $call->getOrderId(),
            $duration,
            $link,
            $call->getId()
        ));
    }

    /**
     * @param SDK\Object\Request $request
     * @return SDK\Object\CallTracking\Call
     */
    private function _getStoredCall(SDK\Object\Request $request) {
        $outerCallId = $request->get("id");
        $callId      = $this->_temporaryStorage->get($outerCallId);
        $this->_temporaryStorage->delete($outerCallId);
        return $this->_callTracking->loadCallById($callId);
    }

    /**
     * @param string $status
     * @return bool
     */
    private function _checkStatus($status) {
        return mb_strlen($status) === 0 ? false : true;
    }

    /**
     * @param string $status
     * @return string
     */
    private function _collationStatus($status) {
        return $this->_validStatuses[$status];
    }

    /**
     * @param string $url
     * @return string|null
     */
    public function _getVisit($url) {
        if (mb_strlen($url) === 0) {
            return null;
        }
        $urlQuery = parse_url($url, PHP_URL_QUERY);
        if (mb_strlen($urlQuery) === 0) {
            return null;
        }
        parse_str($urlQuery, $output);
        if (!array_key_exists('attrs', $output) || mb_strlen($output['attrs']) === 0) {
            return null;
        }
        $decodedAttrs = $this->_utils->jsonDecode($output['attrs']);
        if (!array_key_exists('roistat_visit', $decodedAttrs) || mb_strlen($decodedAttrs['roistat_visit']) === 0) {
            return null;
        }
        return $decodedAttrs['roistat_visit'];
    }

    /**
     * @param string $status
     * @param string $message
     * @return string
     */
    private function _response($status, $message) {
        return json_encode([
            "status" => $status,
            "message" => $message,
        ], JSON_UNESCAPED_UNICODE);
    }

    /**
     * @param string $message
     * @return string
     */
    private function _error($message) {
        return $this->_response("error", $message);
    }

    /**
     * @param string $message
     * @return string
     */
    private function _success($message) {
        return $this->_response("success", $message);
    }
}
<?php

$paths = [
    getenv("SDK_PATH"),
    "../../SDK",
    "../../../marketplace-sdk/SDK",
];
$sdkPath = false;
foreach ($paths as $path) {
    $sdkPath = $path === false ? false : realpath($path);
    if ($sdkPath !== false) {
        break;
    }
}
define("SDK_PATH", $sdkPath);
SDK_PATH || die("SDK path not exists\n");
require_once SDK_PATH . "/Test/autoload.php";
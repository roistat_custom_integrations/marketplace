<?php

namespace RsMarketplace\Calltouch;

use RsMarketplace\SDK;

function t($text) {
    return $text;
}

/**
 * @property Application $_application
 */
class CalltouchTest extends SDK\Test\AbstractTest {

    private $_fullData = [
        'id'            => 729678,
        'callerphone'   => '70000397706',
        'phonenumber'   => '71230000004',
        'url'           => 'https://4dev.online/roistat/?specific_id=563425&attrs={"attrh":1,"ver":170523,"roistat_visit":2}',
        'timestamp'     => '1500287065',
        'status'        => 'successful',
        'duration'      => 70,
        'reclink'       => 'https://api-node2.calltouch.ru/calls-service/RestAPI/17274/calls-diary/calls/23481205/download?clientApiId=988700528ct89302c6312f035c18f8fb871294c49f7',
    ];

    public function testStartCallEndCall() {
        $keys = ['id', 'callerphone', 'phonenumber', 'url', 'timestamp'];
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"success","message":"Event was successfully processed"}', $result);

        $lead = $this->_handlerLeads->getLastLead();
        $this->_assertLead($lead);

        $call = $this->_handlerCallTracking->getLastCall();
        $this->_assertCall($call);
        $this->assertSame($this->_handlerLeads->getLastLeadId(), $call->getOrderId());

        $keys = array_keys($this->_fullData);
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"success","message":"Event was successfully processed"}', $result);

        $updatedCall = $this->_handlerCallTracking->getLastCall();
        $this->_assertCall($updatedCall, true);
        $this->assertSame($call->getId(), $updatedCall->getId());
    }

    public function testEndCall() {
        $keys = array_keys($this->_fullData);
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"success","message":"Event was successfully processed"}', $result);

        $lead = $this->_handlerLeads->getLastLead();
        $this->_assertLead($lead);

        $call = $this->_handlerCallTracking->getLastCall();
        $this->_assertCall($call, true);
        $this->assertSame($this->_handlerLeads->getLastLeadId(), $call->getOrderId());
    }

    public function testNoCaller() {
        $keys = ['id', 'callerphone', 'url', 'timestamp'];
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"error","message":"Caller is undefined"}', $result);

        $lead = $this->_handlerLeads->getLastLead();
        $this->assertSame(null, $lead);

        $call = $this->_handlerCallTracking->getLastCall();
        $this->assertSame(null, $call);
    }

    public function testNoCallee() {
        $keys = ['id', 'callerphone', 'url', 'timestamp'];
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"error","message":"Callee is undefined"}', $result);

        $lead = $this->_handlerLeads->getLastLead();
        $this->assertSame(null, $lead);

        $call = $this->_handlerCallTracking->getLastCall();
        $this->assertSame(null, $call);
    }

    public function testNoCallId() {
        $keys = ['callerphone', 'phonenumber', 'url', 'timestamp'];
        $request = $this->_prepareRequest($keys);
        $result = $this->_application->receiveRequest($request);
        $this->assertSame('{"status":"error","message":"call_id is undefined"}', $result);

        $lead = $this->_handlerLeads->getLastLead();
        $this->assertSame(null, $lead);

        $call = $this->_handlerCallTracking->getLastCall();
        $this->assertSame(null, $call);
    }

    /**
     * @param array $keys
     * @return SDK\Object\Request
     */
    private function _prepareRequest(array $keys) {
        $data = [];
        foreach ($keys as $key) {
            $data[$key] = $this->_fullData[$key];
        }
        return new SDK\Object\Request([], $data, "");
    }

    /**
     * @param SDK\Object\Lead $lead
     */
    private function _assertLead(SDK\Object\Lead $lead) {
        $visit = $this->_getFieldValueById($lead->getFields(), 'roistat');
        $this->assertSame('New lead from calltracking', $lead->getTitle());
        $this->assertSame($this->_fullData['callerphone'], $lead->getPhone());
        $this->assertSame($this->_application->_getVisit($this->_fullData['url']), $visit);
        $this->assertEquals(new \DateTime($this->_fullData['timestamp']), $lead->getCreationDate());
    }

    /**
     * @param SDK\Object\CallTracking\Call $call
     * @param bool $isEndCall
     */
    private function _assertCall(SDK\Object\CallTracking\Call $call, $isEndCall = false) {
        $this->assertSame($this->_fullData['phonenumber'], $call->getCallee());
        $this->assertSame($this->_fullData['callerphone'], $call->getCaller());
        $this->assertEquals(new \DateTime($this->_fullData['timestamp']), $call->getDate());
        $this->assertSame($this->_application->_getVisit($this->_fullData['url']), $call->getVisitId());
        if ($isEndCall) {
            $this->assertSame($this->_fullData['status'], $call->getStatus());
            $this->assertSame($this->_fullData['duration'], $call->getDuration());
            $this->assertSame($this->_fullData['reclink'], $call->getFileUrl());
        }
    }
}
